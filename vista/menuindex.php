<!--Menú del menuindex -->
<div class="navbar-fixed blue"> <!--DEJAR EL MENU FIJO-->
<nav>
    <div class="nav-wrapper blue">
    <a href="#" class="brand-logo right">Menú</a>
    <!--hace el menu adaptable a la pantalla-->
      <a href="#" data-target="menu-responsive" class="sidenav-trigger  btn-large cyan pulse">
      <i class="material-icons">menú</i>
      </a> 
        <ul class="left hide-on-med-and-down ">
        <li><a href="#quienes"> Quienes Somos </a></li>
        <li><a href="#proyecto"> Proyecto </a></li>
        <li><a href="#integrantes"> Integrantes </a></li>
        <li><a href="#preguntas"> Preguntas</a></li>
        <li><a class="modal-trigger" href="#modal1">Ingreso</a></li>
      </ul>
      </div>
      </div>
  </nav>
        <ul class="sidenav" id="menu-responsive" >
        <div class="center promo promo-example">
        <h1>MENU</h1></div>
        <li><a class="sidenav-close" href="#quienes"> Quienes Somos </a></li>
        <li><a class="sidenav-close" href="#proyecto"> Proyecto </a></li>
        <li><a class="sidenav-close" href="#integrantes"> Integrantes </a></li>
        <li><a href="#preguntas"> Preguntas</a></li>
        <li><a class="sidenav-close" class="modal-trigger" href="#modal1">Ingreso</a></li>
      </ul>
